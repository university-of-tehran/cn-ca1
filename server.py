import os
import socket
import shutil
import sys
import json
import base64
from datetime import datetime
from threading import Thread
import time

HOST = "127.0.0.1"
username_to_log_in = None
logged_in_user = None
root_dir = os.getcwd()

def read_config():
    global ctrl_port
    global data_port
    global users
    global accounting
    global logging
    global authorization
    f = open("config.json")
    config = json.load(f)
    f.close()
    authorization = config ["authorization"]
    users = config["users"]
    logging = config["logging"]
    accounting = config["accounting"]
    ctrl_port = config["commandChannelPort"]
    data_port = config["dataChannelPort"]

def update_config_file():
    global ctrl_port
    global data_port
    global users
    global accounting
    global logging
    global authorization
    config = {}
    config["commandChannelPort"] = ctrl_port
    config["dataChannelPort"] = data_port
    config["users"] = users
    config["accounting"] = accounting
    config["logging"] = logging
    config["authorization"] = authorization
    f = open(root_dir+'/'+"config.json", 'w')
    json.dump(config, f)
    f.close()
    

def write_log(new_log):
    if not logging["enable"]:
        return
    with open(str(root_dir + '/' + logging["path"]), "a") as f:
        f.write(f"{datetime.now()} - {new_log}\n")
        

def create_socket():
    try:
        global ds
        global cs
        cs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ds = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as e:
        print("Socket creation error ", str(e))


def bind_socket():
    try:
        global HOST
        global ctrl_port
        global data_port
        global ds
        global cs

        cs.bind((HOST, ctrl_port))
        cs.listen(5)
        print("Socket binded to port", ctrl_port, "listeninig")
        ds.bind((HOST, data_port))
        ds.listen(6)
        print("Socket binded to port", data_port, "listeninig")
    except socket.error as e:
        print("Error in binding socket ", str(e), "\nRetrying...")
        bind_socket()


def sendAccountingAlertEmail(the_user_accounting_info):
    msg = "Your accounting threshold limit has been reached."
    endmsg = "\r\n.\r\n"
    mailserver = ("mail.ut.ac.ir", 25)
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientSocket.connect(mailserver)
    recv = clientSocket.recv(1024)
    recv = recv.decode()
    print("Message after connection request:" + recv)
    if recv[:3] != '220':
        print('220 reply not received from server.')
    heloCommand = 'HELO Amin\r\n'
    clientSocket.send(heloCommand.encode())
    recv1 = clientSocket.recv(1024)
    recv1 = recv1.decode()
    print("Message after HELO command:" + recv1)
    if recv1[:3] != '250':
        print('250 reply not received from server.')

    username = "aminbashiri"
    password = "----"
    base64_str = ("\x00"+username+"\x00"+password).encode()
    base64_str = base64.b64encode(base64_str)
    authMsg = "AUTH PLAIN ".encode()+base64_str+"\r\n".encode()
    clientSocket.send(authMsg)
    recv_auth = clientSocket.recv(1024)
    print(recv_auth.decode())

    mailFrom = "MAIL FROM:<aminbashiri@ut.ac.ir>\n"
    clientSocket.send(mailFrom.encode())
    recv2 = clientSocket.recv(1024)
    recv2 = recv2.decode()
    print("After MAIL FROM command: "+recv2)
    rcptTo = f"RCPT TO:<{the_user_accounting_info['email']}>\r\n"
    clientSocket.send(rcptTo.encode())
    recv3 = clientSocket.recv(1024)
    recv3 = recv3.decode()
    print("After RCPT TO command: "+recv3)
    data = "DATA\r\n"
    clientSocket.send(data.encode())
    recv4 = clientSocket.recv(1024)
    recv4 = recv4.decode()
    print("After DATA command: "+recv4)
    subject = "Subject: Your data limit has been reached.\r\n\r\n" 
    clientSocket.send(subject.encode())
    date = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
    date = date + "\r\n\r\n"
    clientSocket.send(date.encode())
    clientSocket.send(msg.encode())
    clientSocket.send(endmsg.encode())
    recv_msg = clientSocket.recv(1024)
    print("Response after sending message body:"+recv_msg.decode())
    quit = "QUIT\r\n"
    clientSocket.send(quit.encode())
    recv5 = clientSocket.recv(1024)
    print(recv5.decode())
    clientSocket.close()


def handleAccounting(file_size):
    global logged_in_user
    if not accounting["enable"]:
        return True
    the_user_accounting_info = None
    for user in accounting["users"]:
        if user["user"] == logged_in_user["user"]:
            the_user_accounting_info = user
    if int(the_user_accounting_info["size"]) >= file_size:
        the_user_accounting_info["size"] = str(int(the_user_accounting_info["size"]) - file_size)
        if the_user_accounting_info["alert"] and int(the_user_accounting_info["size"]) <= accounting["threshold"]:
            write_log(f"Sending accounting threshold email to {the_user_accounting_info['email']}.")
            Thread(target=sendAccountingAlertEmail, args=(the_user_accounting_info,)).start()
        update_config_file()
        return True
    else:
        if the_user_accounting_info["alert"] and int(the_user_accounting_info["size"]) <= accounting["threshold"]:
            write_log(f"Sending accounting threshold email to {the_user_accounting_info['email']}.")
            Thread(target=sendAccountingAlertEmail, args=(the_user_accounting_info,)).start()
        return False


def socket_accept():
    while True:
        ctrl_conn, address = cs.accept()
        data_conn, address = ds.accept()
        write_log(f"Connection established: Ip: {address[0]}:{address[1]}")
        try:
            Thread(target=send_data, args=(ctrl_conn, data_conn, address)).start()
        except:
            write_log("error in creating thread")


def handleList(ctrl_conn, data_conn):
    datas = os.listdir()
    datas = [i+str("\n") for i in datas]
    size = str(len(datas))
    ctrl_conn.send(str.encode(size))
    for d in datas:
        d = str(d)
        data_conn.send(str.encode(d))
        l = data_conn.recv(10)
    ctrl_conn.send(str.encode("226 List transfer done."))


def handlePwd(ctrl_conn):
    res = os.getcwd()
    ctrl_conn.send(str.encode(f"257 {str(res)}"))


def handleCwd(ctrl_conn, path):
    if not path:
        os.chdir(root_dir)
        ctrl_conn.send(str.encode("250 Successful change."))
    elif not os.path.exists(path):
        ctrl_conn.send(str.encode("404 No such directory."))
    else:
        os.chdir(path)
        ctrl_conn.send(str.encode("250 Successful change."))
    ctrl_conn.send(str.encode(str(os.getcwd())))


def handleDl(ctrl_conn, data_conn, filename):
    how_many = ctrl_conn.recv(20)
    ctrl_conn.send(str.encode("ok"))
    if how_many.decode("utf-8") == "$all_$":
        path = os.getcwd()
        all_files = [f for f in os.listdir(path)
                     if os.path.isfile(os.path.join(path, f))]
        ssize = len(all_files)
        ctrl_conn.send(str.encode(str(ssize)))
        n = ctrl_conn.recv(10)
        for a_file in all_files:
            ctrl_conn.send(str.encode(str(a_file)))
            n = ctrl_conn.recv(10)
            if not handleAccounting(os.path.getsize(str(a_file))):
                data_conn.send(str.encode("425 Can't open data connection."))
            if os.path.normpath(filename) in (os.path.normpath(p) for p in authorization["files"]):
                if logged_in_user["user"] not in authorization["admins"]:
                    data_conn.send(str.encode("550 File unavailable."))
                    continue
            with open(str(a_file), "rb") as f:
                l = f.read(1024)
                while (l):
                    data_conn.send(l)
                    n = ctrl_conn.recv(10)
                    l = f.read(1024)
                write_log(f"User {logged_in_user.get('user')} downloaded file {a_file}.")
                data_conn.send(str.encode("226 Sucessful Download"))
    else:
        if os.path.exists(filename):
            ctrl_conn.send(str.encode("$present$"))
            istry = ctrl_conn.recv(20)
            if istry.decode("utf-8") == "ok":
                if not handleAccounting(os.path.getsize(filename)):
                    data_conn.send(str.encode("425 Can't open data connection."))
                if os.path.normpath(filename) in (os.path.normpath(p) for p in authorization["files"]):
                    if logged_in_user["user"] not in authorization["admins"]:
                        data_conn.send(str.encode("550 File unavailable."))
                        return
                with open(filename, "rb") as f:
                    l = f.read(1024)
                    while (l):
                        data_conn.send(l)
                        n = ctrl_conn.recv(10)
                        print('Sent ', repr(n))
                        l = f.read(1024)
                    data_conn.send(str.encode("226 Sucessful Download"))
                    write_log(f"User {logged_in_user.get('user')} downloaded file {filename}.")
        else:
            ctrl_conn.send(str.encode("404 File not found."))


def handleUl(ctrl_conn, data_conn, filename):
    how_many = ctrl_conn.recv(20)
    ctrl_conn.send(str.encode("ok"))
    if how_many.decode("utf-8") == "$all$":
        ssize = ctrl_conn.recv(20)
        ctrl_conn.send(str.encode("ok"))
        for i in range(int(ssize)):
            fff_name = ctrl_conn.recv(100)
            fff_name = fff_name.decode("utf-8")
            ctrl_conn.send(str.encode("ok"))
            with open('new_from_cli_'+fff_name, 'wb') as f:
                data = data_conn.recv(1024)
                while True:
                    f.write(data)
                    ctrl_conn.send(str.encode("ok"))
                    data = data_conn.recv(1024).decode("utf-8")
                    if data == "226 Sucessful Upload":
                        write_log(f"User {logged_in_user.get('user')} uploaded file {'new_from_cli_'+fff_name}.")
                        print(data)
                        break
    else:
        with open('new_from_cli_'+filename, 'wb') as f:
            data = data_conn.recv(1024)
            while True:
                f.write(data)
                ctrl_conn.send(str.encode("ok"))
                data = data_conn.recv(1024).decode("utf-8")
                if data == "226 Sucessful Upload":
                    write_log(f"User {logged_in_user.get('user')} uploaded file {'new_from_cli_'+filename}.")
                    print(data)
                    break


def handleUsername(ctrl_conn, username):
    global username_to_log_in
    if not username:
        ctrl_conn.send(str.encode("501 Provide a username."))
        return
    username_to_log_in = username
    ctrl_conn.send(str.encode("331 Username okay, need password"))


def handlePassword(ctrl_conn, password):
    global username_to_log_in
    global logged_in_user
    if not password:
        ctrl_conn.send(str.encode("501 Syntax error in parameters or arguments."))
        return
    
    if not username_to_log_in:
        ctrl_conn.send(str.encode("503 Bad sequence of commands."))
        return

    for user in users:
        if username_to_log_in == user["user"] and password == user["password"]:
            logged_in_user = user
            username_to_log_in = None
            write_log(f"User {username_to_log_in} successfuly logged in.")
            ctrl_conn.send(str.encode("230 User logged in, proceed."))
            return
    write_log(f"Invalid username or password for username {username_to_log_in}")
    ctrl_conn.send(str.encode("430 Invalid username or password."))


def handleQuit(ctrl_conn):
    global logged_in_user
    global username_to_log_in
    write_log(f"User {logged_in_user.get('user')} successfuly logged out.")
    logged_in_user = None
    username_to_log_in = None
    ctrl_conn.send(str.encode("221 Successful quit."))


def handleRemove(ctrl_conn, flag, file_address):
    if flag == "-f":
        try:
            shutil.rmtree(file_address)
            ctrl_conn.send(str.encode(f"250 directory {file_address} deleted."))
            write_log(f"User {logged_in_user.get('user')} deleted directory {file_address}.")
        except Exception as e:
            ctrl_conn.send(str.encode(f"500 Error {e}."))
    elif file_address == None:
        try:
            os.remove(flag)
            ctrl_conn.send(str.encode(f"250 file {flag} deleted."))
            write_log(f"User {logged_in_user.get('user')} deleted file {flag}.")
        except Exception as e:
            ctrl_conn.send(str.encode(f"500 Error {e}."))
    else:
        ctrl_conn.send(str.encode("501 Syntax error in parameters or arguments."))


def handleMake(ctrl_conn, flag, file_address):
    if flag == "-i":
        try:
            os.mknod(file_address)
            ctrl_conn.send(str.encode(f"257 file {file_address} created."))
            write_log(f"User {logged_in_user.get('user')} created file {file_address}.")
        except Exception as e:
            ctrl_conn.send(str.encode(f"500 Error {e}."))
    elif file_address == None:
        try:
            os.makedirs(flag)
            ctrl_conn.send(str.encode(f"257 directory {flag} created."))
            write_log(f"User {logged_in_user.get('user')} created directory {flag}.")
        except Exception as e:
            ctrl_conn.send(str.encode(f"500 Error {e}."))
    else:
        ctrl_conn.send(str.encode("501 Syntax error in parameters or arguments."))


def handleHelp(ctrl_conn):
    manuals = [
        "214",
        "USER [name], Its argument is used to specify the user's string, It is used fo user authenctication.",
        "PASS [password], This command should be used after the USER command to specify the password of the use.",
        "LIST, Lists files of the current directory.",
        "PWD, Prints the current directory.",
        "CWD [directory], Changes the current directory to specified directory you can use '..' to go up inthe tree.",
        "MKD [flag(-i)] [file or directory name], With '-i' flag creates the desired file, without '-i' flag creates the desired directory.",
        "RMD [flag(-f)] [file or directory name], With '-f' flag deletes the desired directory, without '-f' flag deletes the desired file.",
        "DL [file_name or '.'], Downloads the specified file, given '.' downloads all files of the current directory.",
        "UL [file_name or '.'], Uploads the specified file, given '.' uploads all files of the current directory.",
        "QUIT, Logsout the current loggedin user.",
    ]
    for manual in manuals:
        ctrl_conn.send(str.encode(manual))
        if ctrl_conn.recv(20).decode("utf-8") != "ok":
            ctrl_conn.send(str.encode("$end$"))

    ctrl_conn.send(str.encode("$end$"))


def send_data(ctrl_conn, data_conn, a):
    send_dir = os.getcwd()
    ctrl_conn.send(str.encode(str(send_dir)))
    while True:
        data = ctrl_conn.recv(1024)
        data = data.decode("utf-8")
        r_cmd = data.split(" ")
        cmd = r_cmd[0]

        try:
            arg1 = r_cmd[1]
        except:
            arg1 = None
        
        try:
            arg2 = r_cmd[2]
        except:
            arg2 = None
        if cmd == "USER":
            ctrl_conn.send(str.encode("&ok&"))
            username = arg1
            handleUsername(ctrl_conn, username)
        elif cmd == "PASS":
            ctrl_conn.send(str.encode("&ok&"))
            password = arg1
            handlePassword(ctrl_conn, password)
        elif not logged_in_user:
            write_log(f"Attept to execute {cmd} without login from {a[0]}")
            ctrl_conn.send(str.encode("332 Need account for login."))
        elif cmd == "LIST":
            ctrl_conn.send(str.encode("&ok&"))
            handleList(ctrl_conn, data_conn)
        elif cmd == "DL":
            ctrl_conn.send(str.encode("&ok&"))
            file_name = arg1
            handleDl(ctrl_conn, data_conn, file_name)
        elif cmd == "UL":
            ctrl_conn.send(str.encode("&ok&"))
            file_name = arg1
            handleUl(ctrl_conn, data_conn, file_name)
        elif cmd == "PWD":
            ctrl_conn.send(str.encode("&ok&"))
            handlePwd(ctrl_conn)
        elif cmd == "CWD":
            ctrl_conn.send(str.encode("&ok&"))
            path = arg1
            handleCwd(ctrl_conn, path)
        elif cmd == "QUIT":
            ctrl_conn.send(str.encode("&ok&"))
            handleQuit(ctrl_conn)
        elif cmd == "RMD":
            ctrl_conn.send(str.encode("&ok&"))
            flag = arg1
            file_address = arg2
            handleRemove(ctrl_conn, flag, file_address)
        elif cmd == "MKD":
            ctrl_conn.send(str.encode("&ok&"))
            flag = arg1
            file_address = arg2
            handleMake(ctrl_conn, flag, file_address)
        elif cmd == "HELP":
            ctrl_conn.send(str.encode("&ok&"))
            handleHelp(ctrl_conn)
        else:
            ctrl_conn.send(str.encode("400 Invalid command."))


def main():
    read_config()
    write_log("Server started.")
    create_socket()
    bind_socket()
    socket_accept()


main()