import socket
import os
import sys
HOST = "127.0.0.1"
CTRL_PORT = 8000
DATA_PORT = 8001


ds = socket.socket()
ds.connect((HOST, DATA_PORT))
cs = socket.socket()
cs.connect((HOST, CTRL_PORT))

cur_dir = cs.recv(1024)
cur_dir = cur_dir.decode("utf-8")
while True:
    cmd = input(str("ftp@ ") + str(cur_dir) + " > ")
    cs.send(str.encode(cmd))
    s_cmd = cmd.split(" ")
    cm = s_cmd[0]

    try:
        arg1 = s_cmd[1]
    except:
        arg1 = None

    try:
        arg2 = s_cmd[2]
    except:
        arg2 = None

    if cm == "BYE":
        break
    
    cm_res = cs.recv(1024).decode("utf-8")
    if cm_res != "&ok&":
        print(cm_res)
        continue

    if cm == "LIST":
        size = cs.recv(1)
        print("number of files: ", size.decode("utf-8"))
        file_list = []
        for i in range(int(size)):
            data = ds.recv(1024)
            ds.send(str.encode("ok"))
            if not data:
                break
            file_list.append(data.decode("utf-8"))
            print(data)
        print(cs.recv(1024).decode("utf-8"))
        for file in file_list:
            print(file)
        continue

    if cm == "PWD":
        r = cs.recv(1024).decode("utf-8")
        print(r)
        continue

    if cm == "CWD":
        res = cs.recv(1024).decode("utf-8")
        cur_dir = cs.recv(1024).decode("utf-8")
        print(res)
        continue

    if cm == "DL":
        if arg1 == ".":
            cs.send(str.encode("$all_$"))
            n = cs.recv(10)
            ssize = cs.recv(20)
            cs.send(str.encode("ok"))
            for i in range(int(ssize)):
                fff_name = cs.recv(100)
                fff_name = fff_name.decode("utf-8")
                cs.send(str.encode("ok"))
                data = ds.recv(1024)
                if data.decode("utf-8") == "425 Can't open data connection.":
                    print(data.decode("utf-8"))
                    break
                if data.decode("utf-8") == "550 File unavailable.":
                    print(data.decode("utf-8"))
                    continue
                with open('new_from_serv_'+fff_name, 'wb') as f:
                    while True:
                        f.write(data)
                        cs.send(str.encode("ok"))
                        data = ds.recv(1024)
                        if data.decode("utf-8") == "226 Sucessful Download":
                            print(data.decode("utf-8"))
                            break
        else:
            cs.send(str.encode("$one_$"))
            n = cs.recv(10)
            should_try = cs.recv(20)
            if should_try.decode("utf-8") == "$present$":
                cs.send(str.encode("ok"))
                data = ds.recv(1024)
                if data.decode("utf-8") == "425 Can't open data connection.":
                    print(data.decode("utf-8"))
                    continue
                if data.decode("utf-8") == "550 File unavailable.":
                    print(data.decode("utf-8"))
                    continue
                with open('new_'+arg1, 'wb') as f:
                    while True:
                        f.write(data)
                        cs.send(str.encode("ok"))
                        data = ds.recv(1024)
                        if data.decode("utf-8") == "226 Sucessful Download":
                            print(data.decode("utf-8"))
                            break
            else:
                print(should_try.decode("utf-8"))
        continue

    if cm == "UL":
        if arg1 == ".":
            cs.send(str.encode("$all$"))
            n = cs.recv(10)
            path = os.getcwd()
            all_files = [f for f in os.listdir(path)
                         if os.path.isfile(os.path.join(path, f))]

            ssize = len(all_files)
            cs.send(str.encode(str(ssize)))
            n = cs.recv(10)
            for a_file in all_files:
                cs.send(str.encode(str(a_file)))
                n = cs.recv(10)
                with open(str(a_file), "rb") as f:
                    l = f.read(1024)
                    while (l):
                        ds.send(l)
                        n = cs.recv(10)
                        print('Sent ', repr(n))
                        l = f.read(1024)
                    ds.send(str.encode("226 Sucessful Upload"))
        else:
            if arg1 == "":
                print("provide a filename")
            else:
                cs.send(str.encode("$one$"))
                cur_path = os.getcwd()
                if os.path.exists(arg1):
                    with open(arg1, "rb") as f:
                        l = f.read(1024)
                        while (l):
                            ds.send(l)
                            n = cs.recv(10)
                            print('Sent ', repr(n))
                            l = f.read(1024)
                        ds.send(str.encode("226 Sucessful Upload"))
                else:
                    print("No Such file ", arg1)
        continue
    
    if cm == "USER":
        n = cs.recv(1024).decode("utf-8")
        print(n)
        continue
        
    if cm == "PASS":
        n = cs.recv(1024).decode("utf-8")
        print(n)
        continue
    
    if cm == "QUIT":
        n = cs.recv(1024).decode("utf-8")
        print(n)
        continue

    if cm == "RMD":
        n = cs.recv(1024).decode("utf-8")
        print(n)
        continue

    if cm == "MKD":
        n = cs.recv(1024).decode("utf-8")
        print(n)
        continue

    if cm == "HELP":
        data = cs.recv(1024).decode("utf-8")
        while data != "$end$":
            cs.send(str.encode("ok"))
            print("\n", data)
            data = cs.recv(1024).decode("utf-8")
        continue

    message = cs.recv(1024)
    print(message.decode("utf-8"))